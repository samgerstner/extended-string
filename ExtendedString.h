#ifndef EXTENDED_STRING_H
#define EXTENDED_STRING_H

	//Include Statements
	#include <string.h>
	#include <stdbool.h>
	#include <stdlib.h>
	
	//Constant Declarations
	#define NULL_CHAR '\0'
	
	//Function Prototypes
	bool findSubstring(const char *testStr, const char *searchStr);
	void getSubstring(char *destStr, const char *sourceStr, int startIndex, int endIndex);
	void setStrToLowerCase(char *destStr, const char *sourceStr);
	char toLowerCase(char testChar);

#endif