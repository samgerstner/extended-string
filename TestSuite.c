#include "ExtendedString.h"
#include <stdio.h>

int main()
{
	//Initialize Method
	char *string = "This is a string!";
	char tempStr[30];
	
	//Print start block
	printf("=-=-=-=-=-=-=-=-=-=-=-=--=-=\n");
	printf("Begin ExtendedString Testing\n");
	printf("=-=-=-=-=-=-=-=-=-=-=-=--=-=\n\n");
	
	//Test findSubstring
	if(findSubstring(string, "string"))
	{
		printf("\"This is a string.\" contains \"string\".\n");
	}
	
	//Test getSubString
	getSubstring(tempStr, string, 1, 5);
	printf("Substring of \"This is a string.\" from 1 to 5 is: %s\n", tempStr);
	
	//Test setStrToLowerCase
	setStrToLowerCase(tempStr, "I AM SCREAMING!");
	printf("\"I AM SCREAMING\" all lower case is: %s\n", tempStr);
	
	//Print end block
	printf("=-=-=-=-=-=-=-=-=-=-=-=--=-=\n");
	printf("End ExtendedString Testing\n");
	printf("=-=-=-=-=-=-=-=-=-=-=-=--=-=\n\n\n");
}