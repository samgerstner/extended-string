#include "ExtendedString.h"

/**
* \brief Attempts to locate a substring within a string.
*
* Returns true if searchStr is found within testStr, or false otherwise.
*
* @param testStr string to find substring within
* @param searchStr string to be found within testStr
* 
* @return true if searchStr is found in testStr, false otherwise.
*/
bool findSubstring(const char *testStr, const char *searchStr)
{
	//Initialize Method
	int testStrLen = strlen(testStr);
	int masterIndex = 0;
	int searchIndex, internalIndex;
	
	//loop across the test string
	while(masterIndex < testStrLen)
	{
		//Set internal loop index to current test string index
		internalIndex = masterIndex;

		//set internal search index to zero
		searchIndex = 0;

		//loop to end of test string
		//while test string/sub string characters are the same
		while(internalIndex <= testStrLen && testStr[internalIndex] == searchStr[searchIndex])
		{
			//increment test string, substring indicies
			internalIndex++;
			searchIndex++;

			//check for end of substring (search completed)
			if(searchStr[searchIndex] == NULL_CHAR)
			{
				//return beginning location of sub string
				return true;
			}
		}

		//Increment current beginnin location index
		masterIndex++;
	}
	
	return false;
}

/**
* \brief Gets substring from string.
*
* Gets substring at specified indicies (inclusive) from string.
*
* @param destStr string object the substring should be placed in
* @param sourceStr string to derive the substring from
* @param startIndex starting index of substring
* @param endIndex ending index of substring
*/
void getSubstring(char *destStr, const char *sourceStr, int startIndex, int endIndex)
{
	//Initialize Method
	int substringLength = (endIndex - startIndex) + 1;
	char *tempStr = (char*) malloc(sizeof(char) * (substringLength + 1));
	int sourceIndex = startIndex, substringIndex = 0;
	
	//Loop across sourceStr
	while(sourceIndex <= endIndex && sourceStr[sourceIndex] != NULL_CHAR)
	{
		//copy current character to tempStr
		tempStr[substringIndex] = sourceStr[sourceIndex];
		
		//increment indicies
		substringIndex++;
		sourceIndex++;
	}
	
	//Copy tempStr to destStr
	strcpy(destStr, tempStr);
	
	//Free memory from tempStr
	free(tempStr);
}

/**
* \brief Sets a string to lower case.
*
* @param destStr string object the new string should be placed in
* @param sourceStr the string to set to lower case
*/
void setStrToLowerCase(char *destStr, const char *sourceStr)
{
	//Initialize Method
	int stringLength = strlen(sourceStr), index = 0;
	char *tempStr = (char*) malloc(sizeof(char) * (stringLength + 1));
	
	//loop across sourceStr
	while(index < stringLength)
	{
		//Set char in tempStr
		tempStr[index] = toLowerCase(sourceStr[index]);
		
		//Increment index
		index++;
	}
	
	//copy tempStr to destStr
	strcpy(destStr, tempStr);
	
	//Free memory from tempStr
	free(tempStr);
}

/**
* \breief Sets character to lawer case if it is upper case.
*
* @param testChar char to convert to lower case if needed
*
* @return testChar converted to lower case if needed
*/
char toLowerCase(char testChar)
{
	if(testChar >= 'A' && testChar <= 'Z')
	{
		return (char) testChar + 32;
	}
	
	return testChar;
}