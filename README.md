# Extended String
A suite of functions written in the C language intended to expand upon the existing string.h file.

## Available Functions
bool findSubstring(const char *testStr, const char *searchStr)

void getSubstring(char *destStr, const char *sourceStr, int startIndex, int endIndex)

void setStrToLowerCase(char *destStr, const char *sourceStr)

char toLowerCase(char testChar)

## Documentation
Coming Soon