CC = gcc
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAGS = -Wall $(DEBUG)

output: ExtendedString.o TestSuite.o
	$(CC) $(LFLAGS) ExtendedString.o TestSuite.o -o runTestSuite
	
ExtendedString.o: ExtendedString.c ExtendedString.h
	$(CC) $(CFLAGS) ExtendedString.c
	
TestSuite.o: TestSuite.c
	$(CC) $(CFLAGS) TestSuite.c
	
clean:
	\rm *.o runTestSuite